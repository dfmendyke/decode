

GPP = g++
RM = rm
target = decode

$(target): main.o
	$(GPP) -o $@ $<
	./$(target)

main.o: main.cc ; $(GPP) -c -o $@ $<

.PHONY : clean
clean: ; $(RM) --force *.o $(target)
