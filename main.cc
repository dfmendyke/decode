
#include <sstream>  // std::stringstream
#include <iostream>
using namespace std;


void decode( const string& data ) {

  istringstream stream( data );
  string line;
  char single = 0x0;
  string code;
  uint k = 0;

  cout << data << " : ";

  while ( !stream.eof() ) {

    size_t iter = 0x0;
    code.clear();
    stream >> k >> single;

    do {
      stream >> single;
      code += single;
    } while( single != ']' );
    code.pop_back();  // remove ']' from code

    for ( iter = 0; iter < k; ++iter ) cout << code;

  };  // end while not stream eof

  cout << endl;

};  // end decode


// To execute C++, please define "int main()"
int main() {

  //3[a2[c]]
  //
  string data[ 3 ] = { "3[a]2[bc]", "2[b]4[x]", "2[y]2[z]2[a]" };

  cout << endl << "Coded string : Decoded string" << endl;
  for ( auto& iter : data ) decode( iter );
  cout << endl;

  return 0;

};  // end main
